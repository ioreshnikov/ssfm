from scipy import *


def fwhm(t, f):
    if len(f.shape) == 2:
        return fwhm_many(t, f)
    else:
        return fwhm_one(t, f)


def fwhm_one(t, f):
    f = abs(f)**2
    f = f / max(f)
    for i in range(1, len(f)):
        if f[i-1] <= 0.5 and f[i] > 0.5:
            t1 = t[i-1]
            t2 = t[i]
            f1 = f[i-1]
            f2 = f[i]
            left = t1 + (0.5 - f1)/(f2 - f1)*(t2 - t1)
            break
    for i in reversed(range(0, len(f) - 1)):
        if f[i+1] <= 0.5 and f[i] > 0.5:
            t1 = t[i]
            t2 = t[i+1]
            f1 = f[i]
            f2 = f[i+1]
            right = t1 + (0.5 - f1)/(f2 - f1)*(t2 - t1)
            break
    return right - left


def fwhm_many(t, f):
    n = f.shape[0]
    fwhms = zeros(n)
    for i in range(n):
        fwhms[i] = fwhm_one(t, f[i, :])
    return fwhms
