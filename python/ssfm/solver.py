from math import factorial
from scipy import *


import functools
import sys
from scipy.integrate import ode
from scipy.fftpack import fft, ifft, fftfreq, fftshift


def genericdnl(z, t, input, dispersion, nonlinearity):
    """
    Base solver integrating NLSE-like equations with constant
    (in z) dispersion and nonlinearity.
    """
    # Shift z to start from zero.
    minz = z[0]
    z = z - minz

    # Find the parameters of z and t grids.
    nz = len(z)
    nt = len(t)
    dt = t[1] - t[0]

    # Calculate the frequency scale and cache the dispersion operator
    # in Fourier domain.
    f = 2*pi * fftfreq(nt, dt)
    d = dispersion(f)

    # We're going to integrate a modified spectrum that we will call
    # 'spectrum_'. It is defined as:
    # spectrum = exp(1j * d * z) * spectrum_
    # Initial state of 'spectrum_ is just initial 'spectrum'.
    spectrum = ifft(input)
    spectrum_ = spectrum

    # Define right-hand's side of the equation.
    def rhs(z, spectrum_):
        exp_ = exp(1j * d * z)
        state = fft(exp_ * spectrum_)
        return 1j * 1/exp_ * ifft(nonlinearity(state))

    # Create and configure the solver.
    solver = ode(rhs)
    solver.set_integrator("zvode",
                          rtol=1E-6,
                          atol=1E-12,
                          nsteps=2500)
    solver.set_initial_value(spectrum_, 0)

    # Integrating.
    spectra_ = zeros((nz, nt), dtype=complex)
    spectra_[0, :] = spectrum_
    for i in range(1, nz):
        sys.stdout.write("\rIntegrating: %-3.2f%%" % (100 * i/nz))
        spectra_[i, :] = solver.integrate(z[i])
    sys.stdout.write("\r")

    # Some post-processing is needed: calculating unmodified spectra
    # and translating back to temporal domain.
    spectra = zeros((nz, nt), dtype=complex)
    states = zeros((nz, nt), dtype=complex)
    for i in range(0, nz):
        spectra[i, :] = exp(1j * d * z[i]) * spectra_[i, :]
        states[i, :] = fft(spectra[i, :])
        spectra[i, :] = 1/nt * fftshift(spectra[i, :])
    f = fftshift(f)

    # Shift z back.
    z = z + minz

    return z, t, f, states, spectra


def kerr(gamma, state):
    """
    Kerr nonlinearity.
    """
    return gamma * abs(state)**2 * state


def sod(beta2, f):
    """
    Second-order dispersion.
    """
    return 1/2 * beta2 * f**2


def hod(betas, f):
    """
    Higher-order dispersion.
    """
    op = zeros(f.shape)
    for n, beta in enumerate(betas, start=2):
        op = op + 1/factorial(n) * beta * f**n
    return op


def nlse(z, t, input, beta2, gamma):
    dispersion = functools.partial(sod, beta2)
    nonlinearity = functools.partial(kerr, gamma)
    return genericdnl(z, t, input, dispersion, nonlinearity)


def nlsehod(z, t, input, betas, gamma):
    dispersion = functools.partial(hod, betas)
    nonlinearity = functools.partial(kerr, gamma)
    return genericdnl(z, t, input, dispersion, nonlinearity)


def nlsehodla(z, t, input, betas, gamma):
    dispersion = functools.partial(hod, betas)

    amplitude = 200
    width = 0.01 * (t.max() - t.min())
    absorber = (amplitude / cosh((t - t.min()) / width) +
                amplitude / cosh((t - t.max()) / width))

    def nonlinearity(state):
        return 1j * absorber * state + kerr(gamma, state)

    return genericdnl(z, t, input, dispersion, nonlinearity)


if __name__ == "__main__":
    # To test whether the solver is correct we simulate second order
    # soliton propagation.
    import matplotlib.pyplot as plot
    import view

    t = linspace(-10, +10, 2**9)
    z = linspace(0, 8, 2**8)
    input = 2.0 * 1/cosh(t)
    z, t, f, states, spectra = nlse(z, t, input, -1, +1)

    view.timedomain(z, t, states)
    view.freqdomain(z, f, spectra)
    plot.show()
