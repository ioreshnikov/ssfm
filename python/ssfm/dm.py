from scipy import *

import functools
import ssfm.solver as solver
import ssfm.util as util
import sys


def dm(numperiods, ppsegment,
       t, input, segmentlens, dispersions, nonlinearities):
    """
    A wrapper around base solver allowing to integrate dispersion
    managed systems.
    """
    numsegments = len(segmentlens)
    ppperperiod = numsegments * ppsegment

    nz = numperiods * ppperperiod
    nt = len(t)

    z = zeros(nz)
    states = zeros((nz, nt), dtype=complex)
    spectra = zeros((nz, nt), dtype=complex)

    z_ = [0]
    for p in range(0, numperiods):
        for s in range(0, numsegments):
            sys.stdout.write("\rPeriod %d, segment %d\n" % (p+1, s+1))

            z_ = z_[-1] + linspace(0, segmentlens[s], ppsegment)
            dispersion = dispersions[s]
            nonlinearity = nonlinearities[s]

            z_, t, f, states_, spectra_ = solver.genericdnl(
                z_, t, input, dispersion, nonlinearity)

            start = s * ppsegment + p * ppperperiod
            stop = (s + 1) * ppsegment + p * ppperperiod
            z[start:stop] = z_
            states[start:stop, :] = states_
            spectra[start:stop, :] = spectra_

            input = states_[-1, :]

    return z, t, f, states, spectra


def dmnlse(numperiods, ppsegment,
           t, input, segmentlens, betas, gammas):
    """
    A solver integrating NLSE in a DM fiber.
    """
    dispersions = []
    nonlinearities = []
    numsegments = len(segmentlens)

    for s in range(0, numsegments):
        beta2 = betas[s]
        gamma = gammas[s]

        d = functools.partial(solver.sod, beta2)
        n = functools.partial(solver.kerr, gamma)
        dispersions.append(d)
        nonlinearities.append(n)

    return dm(numperiods, ppsegment,
              t, input, segmentlens, dispersions, nonlinearities)


def dmnlsehod(numperiods, ppsegment,
              t, input, segmentlens, betas, gammas):
    """
    A solver integrating NLSE with HOD terms in a DM fiber. Betas is expected to be the following:
    betas = [[beta2, beta3, ..., betan],
             [beta2, beta3, ..., betan],
             ...
             [beta2, beta3, ..., betan]]
    """
    pass


def nijhof(t, input, segmentlens, betas, gammas,
           maxperiods=32, maxiters=32):
    """
    Pulse shape averaging procedure via J.H.B. Nijhof, W. Forysiak, and
    N.J. Doran 'The Averaging Method for Finding Exactly Periodic
    Dispersion-Managed Solitons'
    """
    ppsegment = 3
    initial_energy = trapz(abs(input)**2)

    _0 = abs(t).argmin()
    it = 0
    while True:
        # Step 1. Propagate until we determine the points of minimum
        # and maximum FWHM.
        fwhms = zeros(maxperiods)
        states = zeros((maxperiods, len(t)), dtype=complex)
        minfwhm = None
        maxfwhm = None
        minstate = input
        maxstate = input
        p = 0
        while True:
            sys.stdout.write('Iteration %d, Period %d\n' % (it, p))
            # Measure FWHM and store it into a list.
            fwhm = util.fwhm(t, input)
            states[p, :] = input
            fwhms[p] = fwhm
            if p >= 2:
                # Starting with the third period we look for local
                # minima and maxima.
                if fwhms[p-1] > fwhms[p-2] and fwhms[p-1] >= fwhms[p]:
                    if maxfwhm is None or fwhms[p-1] >= maxfwhm:
                        maxfwhm = fwhms[p-1]
                        maxstate = states[p-1]
                        sys.stdout.write(
                            "Local maximum reached at FWHM=%.6f\n" % maxfwhm)
                if fwhms[p-1] < fwhms[p-2] and fwhms[p-1] <= fwhms[p]:
                    if minfwhm is None or fwhms[p-1] <= minfwhm:
                        minfwhm = fwhms[p-1]
                        minstate = states[p-1]
                        sys.stdout.write(
                            "Local minimum reached at FWHM=%.6f\n" % minfwhm)
                # If both are found we stop the loop and construct the
                # next iteration.
                if minfwhm is not None and maxfwhm is not None:
                    break
            # If neither is found we propagate a period forward.
            z, t, _, states_, _ = dmnlse(
                1, ppsegment, t, input, segmentlens, betas, gammas)
            input = states_[-1, :]
            p = p + 1
            if p >= maxperiods:
                # If we have reached the end of the fiber there are
                # three possibilities:
                sys.stdout.write("Maximum period count reached.\n")
                if minfwhm is None and maxfwhm is None:
                    # 1. The plot is flat. Return the input.
                    return states_[p-1, :]
                if minfwhm is None:
                    # 2. We started at the minimum.
                    minstate = states_[0, :]
                    sys.stdout.write(
                        "Assuming local minimum at the input with FWHM=%.6f\n"
                        % util.fwhm(t, minstate))
                    break
                if maxfwhm is None:
                    # 3. We started at the maximum.
                    maxstate = states_[0, :]
                    sys.stdout.write(
                        "Assuming local maximum at the input with FWHM=%.6f\n"
                        % util.fwhm(t, maxstate))
                    break
        input = minstate * conj(minstate[_0]) / abs(minstate[_0]) \
              + maxstate * conj(maxstate[_0]) / abs(maxstate[_0])
        energy = trapz(abs(input)**2)
        input = input * sqrt(initial_energy / energy)
        it = it + 1
        if it >= maxiters:
            return input
