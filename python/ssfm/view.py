from math import *
from scipy import *
import matplotlib.pyplot as plot


def show():
    plot.show()


def viewer(y, x, z,
           db=False,
           dbmin=-50,
           cmap="gnuplot",
           xlabel="t",
           ylabel="z",
           skip=None):
    if skip is not None:
        x = x[::skip]
        z = z[:, ::skip]
    im = abs(z)
    im = im / im.max()
    if db:
        im = 10 * log10(im)

    plot.figure()
    plot.pcolormesh(x, y, im, cmap=cmap, rasterized=True)
    plot.xlim([min(x), max(x)])
    plot.ylim([min(y), max(y)])
    plot.xlabel(xlabel)
    plot.ylabel(ylabel)
    plot.colorbar()
    if db:
        plot.clim([dbmin, 0])


def timedomain(z, t, states, **kwargs):
    viewer(z, t, states, cmap="afmhot", **kwargs)


def freqdomain(z, f, spectra, **kwargs):
    viewer(z, f, spectra, cmap="rainbow", xlabel="f", **kwargs)


def disprelation(f, betas, ymin=-10, ymax=+10):
    disp = 0
    for n, beta in enumerate(betas, start=2):
        disp += 1/factorial(n) * beta * f**n

    plot.figure()
    plot.plot(f, disp)
    plot.plot(f, 1/2 * ones(f.shape))
    plot.ylim(ymin, ymax)
